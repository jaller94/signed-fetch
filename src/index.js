/**
 * Signed Fetch
 * @module signed-fetch
 */

import CustomFetch from 'custom-fetch';
import { sign } from 'activitypub-http-signatures';

/**
 * Creates a fetch-like function that sends signed messages
 */
export default class SignedFetch extends CustomFetch {
	/**
	 * Create a SignedFetch function
	 * @param {object}	keyOptions	Key options
	 * @param {string}	keyOptions.keyId	URI of the public key for verifying the signature
	 * @param {string}	keyOptions.privateKey	The private key to use to sign requests
	 * @param {object}	fetchOptions	Fetch implementation
	 * @param {class}	Request	Constructor for the fetch request object
	 * @param {class}	Response	Constructor for the fetch response object
	 * @param {function}	fetch	The actual fetch function to call
	 */
	constructor({ headers=[], keyId, privateKey }, { Request, Response, fetch }){
		super((request, signal) => {
			const headerMap = Object.fromEntries(headers.map(h=>[h, request.headers.get(h) ]));
			const signature = sign(
				{
					privateKey: privateKey,
					publicKeyId: keyId
				},
				{
					url: request.url,
					method: request.method,
					headers: headerMap
				}
			);

			request.headers.append('date', headerMap.date);
			request.headers.append('signature', signature);
			
			return fetch(request, {
				signal
			});
		}, { Request, Response });
	}
}

// Can't use in browser
// https://github.com/whatwg/fetch/issues/156
// https://github.com/solid/authentication-panel/issues/182#issuecomment-157519949