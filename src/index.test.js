import SignedFetch from './index.js';
import { Request, Response } from 'node-fetch';
import { expect, jest } from '@jest/globals';
import keys from './keys.fixture.js';

test('Signs doc', async ()=>{
	const response = new Response();
	const fetch = jest.fn(()=>response);
	const keyId = 'http://example.com/key';
	const privateKey = keys.private;
	const signal = {
		name: 'signal',
		[Symbol.toStringTag]: 'AbortSignal'
	};
	const request = new Request('http://example.com', { signal });

	const signedFetch = new SignedFetch({ keyId, privateKey }, { Request, Response, fetch});

	expect(await signedFetch(request, { signal })).toBe(response);

	const date = new Date().toUTCString();
	
	delete signal [Symbol.toStringTag];

	expect(fetch).toHaveBeenCalledWith(expect.any(Request), {
		signal
	});
	const r = fetch.mock.calls[0][0];
	expect(r.url).toEqual(request.url);
	expect(Object.fromEntries(r.headers.entries())).toEqual({
		date,
		signature: expect.any(String)
	});
});

test('Adds other headers', async ()=>{
	const response = new Response();
	const fetch = jest.fn(()=>response);
	const keyId = 'http://example.com/key';
	const privateKey = keys.private;
	const signal = {
		name: 'signal',
		[Symbol.toStringTag]: 'AbortSignal'
	};
	const digest = 'digest';
	const request = new Request('http://example.com', { headers: { digest }, signal });

	const signedFetch = new SignedFetch({ keyId, privateKey, headers: ['digest'] }, { Request, Response, fetch});

	expect(await signedFetch(request, { signal })).toBe(response);

	const date = new Date().toUTCString();
	
	delete signal [Symbol.toStringTag];
	expect(fetch).toHaveBeenCalledWith(expect.any(Request), {
		signal
	});

	const r = fetch.mock.calls[0][0];
	expect(r.url).toEqual(request.url);
	expect(Object.fromEntries(r.headers.entries())).toEqual({
		date,
		signature: expect.any(String),
		digest: 'digest'
	});
});

