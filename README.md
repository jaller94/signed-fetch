# Signed Fetch

A fetch wrapper that sends signed requests, according to 
the [Signing HTTP Messages draft 08](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-08).

**This has not been tested in production or with other servers yet.**
If you decide to try it and find any bugs please let me know.

```javascript
import SignedFetch from 'signed-fetch';
import * as nodeFetch from 'node-fetch';

// You should make the public key available at this URL
const publicKeyId = 'http://my-site.example/@paul#main-key';
const privateKey = `-----BEGIN RSA PRIVATE KEY-----
${process.env.PRIVATE_KEY}
-----END RSA PRIVATE KEY-----`;

const fetch = new SignedFetch(
	{
		keyId: publicKeyId,
		privateKey: privateKey
	},
	{
		...nodeFetch,
		fetch: nodeFetch.default
	}
);

const res = await fetch('https://mastodon.social/user/gargron')
```

## Dependencies
 - activitypub-http-signatures: ^1.0.0
 - custom-fetch: ^1.0.0
<a name="module_signed-fetch"></a>

## signed-fetch
Signed Fetch


* [signed-fetch](#module_signed-fetch)
    * [module.exports](#exp_module_signed-fetch--module.exports) ⏏
        * [new module.exports(keyOptions, fetchOptions, Request, Response, fetch)](#new_module_signed-fetch--module.exports_new)

<a name="exp_module_signed-fetch--module.exports"></a>

### module.exports ⏏
Creates a fetch-like function that sends signed messages

**Kind**: Exported class  
<a name="new_module_signed-fetch--module.exports_new"></a>

#### new module.exports(keyOptions, fetchOptions, Request, Response, fetch)
Create a SignedFetch function


| Param | Type | Description |
| --- | --- | --- |
| keyOptions | <code>object</code> | Key options |
| keyOptions.keyId | <code>string</code> | URI of the public key for verifying the signature |
| keyOptions.privateKey | <code>string</code> | The private key to use to sign requests |
| fetchOptions | <code>object</code> | Fetch implementation |
| Request | <code>class</code> | Constructor for the fetch request object |
| Response | <code>class</code> | Constructor for the fetch response object |
| fetch | <code>function</code> | The actual fetch function to call |

